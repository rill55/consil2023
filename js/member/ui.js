function open_menu2(i){
	$("#snb_bg").fadeIn();
	$(".snb_2deps").fadeIn();
	$("." + i).css();
}

function open_menu(i, n){
	$("#snb_bg").fadeIn();
	$(".snb_2deps").fadeIn();
	for(var j = 1; j < 5; j++){
		if(i==j){
			$("#sub0" + i).attr("src",$("#sub0" + i).attr("src").replace("off.png","onl.png"));
			$("#snbBg_"+i).attr('class','snb_2deps onbg');
		}else{
			$("#sub0" + j).attr("src",$("#sub0" + j).attr("src").replace("onl.png","off.png"));
			$("#snbBg_"+j).removeClass('snb_2deps onbg');
			$("#snbBg_"+j).addClass('snb_2deps');
		}
	}
}

function setTwitter(name_tilte,url_page) {
	var page = "http://twitter.com/home?status=" + encodeURIComponent(name_tilte) + " " + encodeURIComponent(url_page)
	window.open(page,"registTwitter")
	return
}
function setFaceBook(name_tilte,url_page) {
	var page = "http://www.facebook.com/sharer.php?u=" + encodeURIComponent(name_tilte) + "&t=" + encodeURIComponent(url_page);
	window.open(page,"registFacebook")
	return
}


/*function close_menu(){
	$("#snb_bg").fadeOut();
	$(".snb_2deps").fadeOut();
	for(var j = 1; j < 5; j++){
		$("#sub0" + j).attr("src",$("#sub0" + j).attr("src").replace("onl.png","off.png"));
	}
}*/
function goTwitter(){
	var url = encodeURIComponent(location.href);
	var msg = encodeURIComponent(document.title);
	var href ="http://twitter.com/share?url="+url+"&text="+msg;
	var a = window.open(href,'twitter','');
		if(a){
			a.focus();
		}
	}
function goFaceBook(){
	var url = encodeURIComponent(location.href);
	var msg = encodeURIComponent(document.title);
	var href ="http://www.facebook.com/sharer.php?u="+url+"&t="+msg;
	var a = window.open(href,'facebook','');     
}
function goMeday(){
	var url = encodeURIComponent(location.href);
	var msg = encodeURIComponent(document.title);
	var href ="http://me2day.net/posts/new?new_post[body]="+msg+":"+url;
	var a = window.open(href,'me2day','');
}

$(document).ready(function(){
	$("#gnb>li>a").on('mouseover', function(){
		$(".lnb").removeClass("on");
		$('#header').addClass('dim');
		$(this).addClass("active");
		$(this).next().addClass("on");
	});
	$("#gnb>li").on('mouseleave', function(){		
		$(".lnb").removeClass("on");
		$('#header').removeClass('dim');
		$("#gnb a").removeClass("active");
	});
	$(".btn-search-detail").on("click",function(){
		$(".detail-box").stop().slideToggle(300);
		$(".search-tit").slideToggle(10);
		$(".search-box").toggleClass('indent');
	});
	$('.search_utill').on('click', function(){
		$(this).toggleClass('view');
		$('.w-search').toggleClass('on');
	})
	if ($(window).width() < 990) {
		$('.search_utill').removeClass('view');
		$('.w-search').removeClass('on');
	}
	$('.btn-list').on('click', function(){
		$('.board-icon a').removeClass('active');
		$(this).addClass('active');
		$('.thumb-list').hide();
		$('.board-list').show();
	})
	$('.btn-thumb').on('click', function(){
		$('.board-icon a').removeClass('active');
		$(this).addClass('active');
		$('.board-list').hide();
		$('.thumb-list').show();
	})
	$('.tboard1 .report-cont>h2>a').on('click', function(){
		$('.tboard1 .report-cont').removeClass('on');
		$('.tboard1 .cont').hide();		
		$(this).parent().parent().addClass('on');
		$(this).parent().next().show();
	})
	$('.tboard2 .report-cont>h2>a').on('click', function(){
		$('.tboard2 .report-cont').removeClass('on');
		$('.tboard2 .cont').hide();		
		$(this).parent().parent().addClass('on');
		$(this).parent().next().show();
	})
	$('.m-btn-family a').on('click', function(){
		$('.mobile-tab-group').slideToggle();
	});
	$('.quick-menu>a').click(function(){
		$('.quick-menu').toggleClass('on');
	});
	$('.sitemap_utill').on('click', function(){
		$(this).toggleClass('view');
		$('.menu-all').toggleClass('on');
	})
	if ($(window).width() < 990) {
		$('.sitemap_utill').removeClass('view');
		$('.menu-all').removeClass('on');
	}

	$('.latest dl dt a').bind("mouseenter click focus",function(){
		$(this).parent('dt').addClass('on').siblings('dt').removeClass('on');
		return false;
	});
	//푸터 사이트 바로가기
	$(".select-custom").on("click",function(){
		if($(this).hasClass("on") ){
			$(".select-custom").removeClass("on");
			$(".select-view").removeClass("on");		
		} else {
			$(".select-custom").removeClass("on");
			$(".select-view").removeClass("on");		
			$(this).addClass("on");		
			$(this).next().addClass("on");		
		}
		
	});

});

// 모바일 GNB
$(document).on('click', '.btn-mobile-menu', function() {
	if ($(window).width() > 990) {
		$('.mobile-menu-area').stop().slideDown({
			duration : 600,
			easing : 'easeOutQuint',
			complete : function() { $(this).addClass('allmenu-opened'); }
		});
	} else {
		$('.mobile-menu-area').show().stop().animate({'right' : 0, 'opacity' : 1}, {
			duration : 600,
			easing : 'easeOutQuint',
			complete : function() { $(this).removeClass('allmenu-opened'); }
		});
		bodyScroll('lock');
	}
	return false;
}).on('click', '.btn-menu-close', function() {
	if ($(window).width() > 990) {
		$('.mobile-menu-area').stop().slideUp({
			duration : 600,
			easing : 'easeOutQuint',
			complete : function() { $(this).removeAttr('style').removeClass('allmenu-opened'); }
		});
		$('#mobile-header .mobile-search').css('z-index','');
		$('#mobile-header .mobile-search').hide;
	} else {
		bodyScroll('unlock');
		$('.mobile-menu-area').stop().animate({'right' : '-320px', 'opacity' : 0}, {
			duration : 600,
			easing : 'easeOutQuint',
			complete : function() { $(this).removeAttr('style').removeClass('allmenu-opened'); }
		});
	}
}).on('click', '#mobile-gnb>ul>li>a', function() {
	if ($(window).width() < 990) {
		$('#mobile-gnb>ul>li, #mobile-gnb>ul>li>ul>li').removeClass("on");
		$('#mobile-gnb>ul>li>ul, #mobile-gnb>ul>li>ul>li>ul').hide();

		if(!$(this).next("ul").is(":visible")){
			$(this).next("ul").show();
			$(this).parents("li").addClass("on");
		}
		return false;
	}
}).on('click', '#mobile-gnb>ul>li>ul>li>a', function() {
	if ($(window).width() < 990) {
		$('#mobile-gnb>ul>li>ul>li').removeClass("on");
		$('#mobile-gnb>ul>li>ul>li>ul').slideUp();

		if(!$(this).next("ul").is(":visible")){
			$(this).next("ul").slideDown();
			$(this).parents("li").addClass("on");
		}
		
		if($(this).next().prop("tagName") == "UL"){
			return false;
		}
	}
});
// 레이어 배경스크롤 제어
function bodyScroll(stat) {
	if (stat === 'lock') {
		$('body').addClass('modal-opened');
		$('html').on('touchmove', function (e) {
			e.preventDefault();
			e.stopPropagation();
		});
	} else if (stat === 'unlock') {
		$('html').off('touchmove');
		$('body').removeClass('modal-opened');
	}
}

// 상임위원회 레이어 열기
function openStandingCommittee() {
	closeViewBox();
	$("#standing_committee_box").show();
}
function closeViewBox() {
	$(".view-box").hide();
}


// 모바일 검색
$(document).on('click', '.btn-mobile-search', function() {
	$('.mobile-search').show();
	$('.mobile-search input').focus();
	$('#header').addClass('dim');
}).on('click', '.btn-search-close', function() {
	$('.mobile-search').hide();
	$('.btn-mobile-search').focus();
	$('#header').removeClass('dim');
});
$(document).on('click', '.mobile-menu .btn-mobile-search', function() {
	$('#mobile-header .mobile-search').css('z-index','10000');
})
// language
$(document).on('mousedown', '.language .btn-lang', function(e) {
	$('.language ul').stop().slideToggle('easeOutQuint');
	e.preventDefault()
}).on('focus', '.language', function() {
	$('.language ul').stop().slideDown('easeOutQuint');
}).on('focus', '.zoom, .gnb-area', function() {
	$('.language ul').stop().slideUp('easeOutQuint');
});

$(window).scroll(function(){
	var scrollTop = $(document).scrollTop();
	if (scrollTop < 780) {
		scrollTop = 780;		
	} 
	$(".quick-menu").stop();
	$(".quick-menu").animate( { "top" : scrollTop });

});


//leftMenu 봉식
function leftMenu(eq){
	var classOnWrap1 = ".contents_left_menu"; //style = "on" 시킬 곳의 wrap (수정 해야함)
	var classOn1 = ".classOnSwitch"; // style = "on" 적용시킬 곳 (수정 해야함)
	var eqLength1 = jQuery(classOnWrap1 + " " + classOn1).length; //style = "on" 의 개수
	var styleBlockWrap1 = '.contents_right4' //block을 시킬 곳의 wrap (수정 해야함)
	var styleBlock1 = '.content_' //block을 시킬 곳 (숫자를 뺀다) (수정 해야함)
	for (var i = 0; i<eqLength1; i++){
		if(eq == (i +1)){
			jQuery(styleBlockWrap1 + " " + styleBlock1 + (i + 1)).attr("style","display:block");
			jQuery(styleBlockWrap1 + " " + styleBlock1 + (i + 1) + " h4").attr("style","display:block");
			jQuery(styleBlockWrap1 + " " + styleBlock1 + (i + 1) + " ol").attr("style","display:block"); /*0624 추가*/
			jQuery(styleBlockWrap1 + " " + styleBlock1 + (i + 1) + " ol").attr("style","position:relative"); /*0624 추*/
			jQuery(classOn1 + ":eq(" +  i + ")").addClass("on");
		}
		else{
			jQuery(styleBlockWrap1 + " " + styleBlock1 + (i + 1) + " h4").attr("style","display:none");
			jQuery(styleBlockWrap1 + " " + styleBlock1 + (i + 1) + " ol").attr("style","display:none");
			jQuery(styleBlockWrap1 + " " + styleBlock1 + (i + 1)).attr("style","display:none"); /*0624 추가*/
			jQuery(styleBlockWrap1 + " " + styleBlock1 + (i + 1)).attr("style","position:relative"); /*0624 추가*/
			jQuery(classOn1 + ".on:eq(" + i +")").attr("class","classOnSwitch");
		}
	}
	
	$(function(){
		  $('.list_flexible_height2').masonry({
		    // options
		    itemSelector : '.item',
		    columnWidth : 0
		  });
		});
}

function leftMenu2(eq){
	var classOnWrap1 = ".contents_left_menu"; //style = "on" 시킬 곳의 wrap (수정 해야함)
	var classOn1 = ".classOnSwitch"; // style = "on" 적용시킬 곳 (수정 해야함)
	var eqLength1 = jQuery(classOnWrap1 + " " + classOn1).length; //style = "on" 의 개수
	var styleBlockWrap1 = '.contents_right3' //block을 시킬 곳의 wrap (수정 해야함)
	var styleBlock1 = '.chairman_info_' //block을 시킬 곳 (숫자를 뺀다) (수정 해야함)
	for (var i = 0; i<eqLength1; i++){
		if(eq == (i +1)){
			jQuery(styleBlockWrap1 + " " + styleBlock1 + (i + 1)).attr("style","display:block");
			jQuery(classOn1 + ":eq(" +  i + ")").addClass("on");
		}
		else{
			jQuery(styleBlockWrap1 + " " + styleBlock1 + (i + 1)).attr("style","display:none");
			jQuery(classOn1 + ".on:eq(" + i +")").attr("class","classOnSwitch");
		}
	}
}
function leftMenu3(eq){
	var classOnWrap1 = ".contents_left_menu"; //style = "on" 시킬 곳의 wrap (수정 해야함)
	var classOn1 = ".classOnSwitch"; // style = "on" 적용시킬 곳 (수정 해야함)
	var eqLength1 = jQuery(classOnWrap1 + " " + classOn1).length; //style = "on" 의 개수
	var styleBlockWrap1 = '.contents_right4' //block을 시킬 곳의 wrap (수정 해야함)
	var styleBlock1 = '.map_info_' //block을 시킬 곳 (숫자를 뺀다) (수정 해야함)
	for (var i = 0; i<eqLength1; i++){
		if(eq == (i +1)){
			jQuery(styleBlockWrap1 + " " + styleBlock1 + (i + 1)).attr("style","display:block");
			jQuery(classOn1 + ":eq(" +  i + ")").addClass("on");
		}
		else{
			jQuery(styleBlockWrap1 + " " + styleBlock1 + (i + 1)).attr("style","display:none");
			jQuery(classOn1 + ".on:eq(" + i +")").attr("class","classOnSwitch");
		}
	}
}

function lnb(eq){
	var classOnWrap1 = ".lnb_wrap"; //style = "on" 시킬 곳의 wrap (수정 해야함)
	var classOn1 = ".lnb_1deps"; // style = "on" 적용시킬 곳 (수정 해야함)
	var eqLength1 = jQuery(classOnWrap1 + " " + classOn1).length; //style = "on" 의 개수
	var styleBlockWrap1 = '.lnb_1deps'; //block을 시킬 곳의 wrap (수정 해야함)
	var styleBlock1 = '.lnb_2deps_' //block을 시킬 곳 (숫자를 뺀다) (수정 해야함)
	for (var i = 0; i<eqLength1; i++){
		if(eq == (i +1)){
			jQuery(styleBlockWrap1 + " " + styleBlock1 + (i + 1)).attr("style","display:block");
		}
		else{
			jQuery(styleBlockWrap1 + " " + styleBlock1 + (i + 1)).attr("style","display:none");
		}
	}
}

//leftMenu 봉식 끝

/*
 * 팝업 자동 리사이징
 *  - 윈도 환경에 따라 사이즈가 다를 수 있습니다.
 *  - 팝업페이지의 스크립트 최하단에서 실행하십시오.
 *
 * (ex.) window.onload = function(){popupAutoResize();}
*/
function popupAutoResize() {
	if ( navigator.userAgent.indexOf("Linux") < 0 ) {  
		var thisX = parseInt(document.documentElement.scrollWidth);
		var thisY = parseInt(document.documentElement.scrollHeight);
		var maxThisX = screen.width - 50;  
		var maxThisY = screen.height - 50;
		var marginY = 0;
		//var SP2 = (navigator.appVersion.indexOf("MSIE 7.0") != -1);   
		//WindowsXP SP2
		if(navigator.userAgent.indexOf("MSIE 8") > 0) {
			marginY = 78;
		} else if(navigator.userAgent.indexOf("MSIE 7") > 0) {
			marginY = 78;                       
		} else if (navigator.userAgent.indexOf("MSIE 6") > 0) {
			marginY = 58;         
		} else if (navigator.userAgent.indexOf("MSIE 9") > 0) {
			marginY = 78;         
		} else if (navigator.userAgent.indexOf("Firefox") > 0) {
			marginY = 84;         
		} else if (navigator.userAgent.indexOf("Chrome") > 0) {
			marginY = 58;            
			//thisX -= 8;     
		} else { 
			marginY = 50;
		} 
		/*
    if (SP2) { 
      marginY = Number(marginY) - 23;
    }else{    
    }
		 */
		//WindowsVISTA, Windows7
		if(navigator.userAgent.indexOf("Windows NT 6") > 0) { 
			if (navigator.userAgent.indexOf("MSIE") > 0) {
				marginY += 0;    
			} else if (navigator.userAgent.indexOf("Firefox") > 0) {
				marginY += -1;      
				thisX += 8;                
			} else if (navigator.userAgent.indexOf("Chrome") > 0) {
				marginY += 3;
				thisX += 8;            
			} 
		}
		//Windows2000
		if(navigator.userAgent.indexOf("Windows NT 5.0") > 0) {
			if (navigator.userAgent.indexOf("MSIE") > 0) {
				marginY -= 41;         
			} else if (navigator.userAgent.indexOf("Firefox") > 0) {
				marginY -= 3;      
			} 
		}
		if (thisX > maxThisX) {
			window.document.body.scroll = "yes";
			thisX = maxThisX;
		}
		if (thisY > maxThisY - marginY) {
			window.document.body.scroll = "yes";
			thisX += 19;
			thisY = maxThisY - marginY;
		}
		if (arguments[0] != null && arguments[0] != "") {
			window.resizeTo(arguments[0], thisY+marginY);
		} else {
			window.resizeTo(thisX+8, thisY+marginY);
		}  
	}
}

$(function() {
	//sns열기
	$(".sns-btn-share").on("click",function(e){
		$(".sns-btns").toggleClass("on");
	});
	$(document).mouseup(function (e){
		var snsPop = $(".sns-btns.on");
		if(snsPop.has(e.target).length === 0){
			snsPop.removeClass("on");
		}
	});


})

