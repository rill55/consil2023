//PC GNB

function gnbNav(){
	$('#gnb').on('mouseover', function(){
		$('.lnb').addClass('on');
		$('#header').addClass('dim');
	})
	$('#header .inner').on('mouseleave', function(){
		$('.lnb').removeClass('on');
		$('#header').removeClass('dim');
	})
	
	$('#gnb').focusin(function() {
		$('.lnb').addClass('on');
		$('#header').addClass('dim');
	})
	$('.search_utill').focusin(function() {
		$('.lnb').removeClass('on');
		$('#header').removeClass('dim');
	})
}
function menuAll(){
	$('.sitemap_utill').on('click', function(){
		$(this).toggleClass('view');
		$('.menu-all').toggleClass('on');
		$(this).text( $(this).text() == "전체메뉴보기" ? "전체메뉴 닫기" : "전체메뉴보기" );
	})
	if ($(window).width() < 990) {
		$('.sitemap_utill').removeClass('view');
		$('.menu-all').removeClass('on');
	}
}
function wSearch(){
	$('.search_utill').on('click', function(){
		$(this).toggleClass('view');
		$('.w-search').toggleClass('on');
		$("#searchText2").focus();
		$(this).text( $(this).text() == "검색" ? "검색 닫기" : "검색" );
	})
	if ($(window).width() < 990) {
		$('.search_utill').removeClass('view');
		$('.w-search').removeClass('on');
	}
}

// 상임위원회 레이어 열기
function openStandingCommittee() {
	closeViewBox();
	$("#standing_committee_box").show();
}
function closeViewBox() {
	$(".view-box").hide();
}

/* 이전 GNB 스크립트
$(document).on('mouseenter', '#gnb>li', function() {
    if ($(window).width() > 990) {
        gnbOpen($(this));
    }
}).on('mouseleave', '#gnb-area', function() {
    if ($(window).width() > 990) {
        gnbClose();
    }
}).on('focus', '#gnb>li>a', function() {
    if ($(window).width() > 990) {
        gnbOpen($(this).parent());
    }
}).on('focus', '#content', function() {
    if ($(window).width() > 990) {
        gnbClose();
    }
});
function gnbOpen(lnb) {
	$(lnb).addClass('active').siblings().removeClass('active');
	$('#gnb-area').stop().animate({'height' : '460px'}, {duration:300, easing:'easeOutQuint'});
}
function gnbClose() {
	$('#gnb-area').stop().animate({'height' : '140px'}, {
		duration : 300,
		easing : 'easeOutQuint',
		complete : function() { $('#gnb-area').removeAttr('style') }
	});
	$('#gnb>li').removeClass('active');
}
*/



// 모바일 GNB
$(document).on('click', '.btn-mobile-menu', function() {
	if ($(window).width() > 990) {
		$('.mobile-menu-area').stop().slideDown({
			duration : 600,
			easing : 'easeOutQuint',
			complete : function() { $(this).addClass('allmenu-opened'); }
		});
	} else {
		$('.mobile-menu-area').show().stop().animate({'right' : 0, 'opacity' : 1}, {
			duration : 600,
			easing : 'easeOutQuint',
			complete : function() { $(this).removeClass('allmenu-opened'); }
		});
		bodyScroll('lock');
	}
	return false;
}).on('click', '.btn-menu-close', function() {
	if ($(window).width() > 990) {
		$('.mobile-menu-area').stop().slideUp({
			duration : 600,
			easing : 'easeOutQuint',
			complete : function() { $(this).removeAttr('style').removeClass('allmenu-opened'); }
		});
		$('#mobile-header .mobile-search').css('z-index','');
		$('#mobile-header .mobile-search').hide;
	} else {
		bodyScroll('unlock');
		$('.mobile-menu-area').stop().animate({'right' : '-320px', 'opacity' : 0}, {
			duration : 600,
			easing : 'easeOutQuint',
			complete : function() { $(this).removeAttr('style').removeClass('allmenu-opened'); }
		});
	}
}).on('click', '#mobile-gnb>ul>li>a', function() {
	if ($(window).width() < 990) {
		$('#mobile-gnb>ul>li, #mobile-gnb>ul>li>ul>li').removeClass("on");
		$('#mobile-gnb>ul>li>ul, #mobile-gnb>ul>li>ul>li>ul').hide();

		if(!$(this).next("ul").is(":visible")){
			$(this).next("ul").show();
			$(this).parents("li").addClass("on");
		}
		return false;
	}
}).on('click', '#mobile-gnb>ul>li>ul>li>a', function() {
	if ($(window).width() < 990) {
		$('#mobile-gnb>ul>li>ul>li').removeClass("on");
		$('#mobile-gnb>ul>li>ul>li>ul').slideUp();

		if(!$(this).next("ul").is(":visible")){
			$(this).next("ul").slideDown();
			$(this).parents("li").addClass("on");
		}
		
		if($(this).next().prop("tagName") == "UL"){
			return false;
		}
	}
});

// 레이어 배경스크롤 제어
function bodyScroll(stat) {
	if (stat === 'lock') {
		$('body').addClass('modal-opened');
		$('html').on('touchmove', function (e) {
			e.preventDefault();
			e.stopPropagation();
		});
	} else if (stat === 'unlock') {
		$('html').off('touchmove');
		$('body').removeClass('modal-opened');
	}
}

// 모바일 검색
$(document).on('click', '.btn-mobile-search', function() {
	$('.mobile-search').show();
	$('.mobile-search input').focus();
}).on('click', '.btn-search-close', function() {
	$('.mobile-search').hide();
	$('.btn-mobile-search').focus();
});
$(document).on('click', '.mobile-menu .btn-mobile-search', function() {
	$('#mobile-header .mobile-search').css('z-index','10000');
})
// language
$(document).on('mousedown', '.language .btn-lang', function(e) {
	$('.language ul').stop().slideToggle('easeOutQuint');
	e.preventDefault()
}).on('focus', '.language', function() {
	$('.language ul').stop().slideDown('easeOutQuint');
}).on('focus', '.zoom, .gnb-area', function() {
	$('.language ul').stop().slideUp('easeOutQuint');
});

// main scroll
var lastScroll = 0;
$(window).scroll(function(event){
	var scroll = $(this).scrollTop();
	var height = $(document).scrollTop();
	if (scroll > 500){
	   $(".art").removeClass('stay');
	   $(".art2").addClass('stay');
	   $(".toTheTop").show();	   
	} else {
		$(".art").removeClass('stay');
		$(".art1").addClass('stay');
		$(".wheel-event").removeClass('top');
	}
	if (scroll > 1000){
	   $(".wheel-event").attr('href','#main-media');
	   $(".wheel-event.top").attr('href','#top-nav');
	} else {
		$(".wheel-event").attr('href','#main-area');  
	}
	if (scroll > 1500){	    
		$(".wheel-event").addClass('top');
		$(".wheel-event.top").attr('href','#main-area');
		$(".art").removeClass('stay');
	    $(".art3").addClass('stay');
	} else {
	}	
	lastScroll = scroll;
});
$(document).ready(function() { 
	$('.wheel-event, .art a, .toTheTop').click(function () {
		$('html, body').animate({
		  scrollTop: $($.attr(this, 'href')).offset().top
		}, 800);
		return false;
	});
});


// 메인 비주얼 모바일 이미지 변경
$(window).on('resize', function () {
	visualResponsive();
    visualResponsiveNotice();
	gnbNav();
});

$(window).on('load', function () {
    visualResponsive();
    visualResponsiveNotice();
	gnbNav();
	menuAll();
	wSearch();
});

function visualResponsive() {
	if ($(document).width() < 975){
		$('.visual-slider .item img').attr('src', function(index, attr){
			return attr.replace('-pc','-m');
		});
	}else {
		$('.visual-slider .item img').attr('src', function(index, attr){
			return attr.replace('-m','-pc');
		});
	}
}

function visualResponsiveNotice()/*알림판배너*/ {
	if ($(document).width() < 975){
		$('.visual-slider-notice .item img').attr('src', function(index, attr){
			return attr.replace('-pc','-m');
		});
	}else {
		$('.visual-slider-notice .item img').attr('src', function(index, attr){
			return attr.replace('-m','-pc');
		});
	}
}


$(document).ready(function () {
	// 메인 비주얼

	
	var $vstat = $('#visual-section .slide-count');
	var $visualSlider = $('.visual-slider');
    $visualSlider.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
        //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
        var i = (currentSlide ? currentSlide : 0) + 1;
        $vstat.html("<strong>" + "0" + i + "</strong>" + '/' + "0" + slick.slideCount);
    });
	$visualSlider.slick({
		speed : 1000,
		autoplay: true,
		autoplaySpeed: 4000,
		prevArrow: '.visual-ctrl-new .btn-prev',
		nextArrow: '.visual-ctrl-new .btn-next' 
	});

	$('.visual-ctrl-new .btn-prev').focusin(function() {
		$visualSlider.slick('slickPause');
	});
	$('.visual-ctrl-new .btn-next').focusin(function() {
		$visualSlider.slick('slickPause');
	});
	$('.visual-ctrl-new .btn-prev').focusout(function() {
		$visualSlider.slick('slickPlay');
	});
	$('.visual-ctrl-new .btn-next').focusout(function() {
		$visualSlider.slick('slickPlay');
	});
	$('.visual-ctrl-new .btn-pause').click(function() {
		$visualSlider.slick('slickPause');
		$(this).hide().next().show().focus();
	});
	$('.visual-ctrl-new .btn-play').click(function() {
		$visualSlider.slick('slickPlay');
		$(this).hide().prev().show().focus();
	});

	var $visualSlider_notice = $('.visual-slider-notice');
	$visualSlider_notice.slick({
		easing : 'easeOutQuint',
		speed : 1000,
		autoplay: true,
		autoplaySpeed: 3000,
		prevArrow: '.visual-ctrl-notice .btn-prev_w',
		nextArrow: '.visual-ctrl-notice .btn-next_w'
	});
	$('.visual-ctrl-notice .btn-pause_w').click(function() {
		$visualSlider_notice.slick('slickPause');
		$(this).hide().next().show().focus();
	});
	$('.visual-ctrl-notice .btn-play_w').click(function() { 
		$visualSlider_notice.slick('slickPlay');
		$(this).hide().prev().show().focus();
	});
	


	// 실시간 의회 슬라이더 추가
	var $lstat = $('#live-section .slide-count');
	var $liveSlider = $('.live-slider');
	$liveSlider.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
        //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
        var i = (currentSlide ? currentSlide : 0) + 1;
        $lstat.html("<strong>" + "0" + i + "</strong>" + '/' + "0" + slick.slideCount);
		
    });
	$liveSlider.slick({
		autoplay: true,
		vertical: true,
		autoplaySpeed: 3000,
		slidesToShow : 1,
		prevArrow: '.live-ctrl .btn-prev',
		nextArrow: '.live-ctrl .btn-next',
	});
	$('.live-ctrl .btn-pause').click(function() {
		$liveSlider.slick('slickPause');
		$(this).hide().next().show().focus();
	});
	$('.live-ctrl .btn-play').click(function() {
		$liveSlider.slick('slickPlay');
		$(this).hide().prev().show().focus();
	});

		


	// 작은 배너 슬라이더 추가
	var $sstat = $('.sbanner-wrap .slide-count');
	var $smallSlider = $('.sbanner-slider');
    $smallSlider.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
        //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
        var i = (currentSlide ? currentSlide : 0) + 1;
        $sstat.html("<strong>" + "0" + i + "</strong>" + '/' + "0" + slick.slideCount);
    });
	$smallSlider.slick({
		autoplay: true,
		autoplaySpeed: 3000,
		slidesToShow : 1,
		prevArrow: '.sbanner-ctrl .btn-prev',
		nextArrow: '.sbanner-ctrl .btn-next',
	});
	$('.sbanner-ctrl .btn-pause').click(function() {
		$smallSlider.slick('slickPause');
		$(this).hide().next().show().focus();
	});
	$('.sbanner-ctrl .btn-play').click(function() {
		$smallSlider.slick('slickPlay');
		$(this).hide().prev().show().focus();
	});
	
	var $s1stat = $('.sns-youtube-ctrl .slide-count');
	var $snsSlider1 = $('.youtube-slider');
    $snsSlider1.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
        //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
        var i = (currentSlide ? currentSlide : 0) + 1;
        $s1stat.html("<strong>" + "0" + i + "</strong>" + '/' + "0" + slick.slideCount);
    });
	// sns 슬라이더 추가
	var $snsSlider1 = $('.youtube-slider');
	$snsSlider1.slick({
		autoplay: true,
		autoplaySpeed: 3000,
		slidesToShow : 4,
		slidesToScroll : 4,
		prevArrow: '.sns-youtube-ctrl .btn-prev',
		nextArrow: '.sns-youtube-ctrl .btn-next',
		responsive: [ // 반응형 웹 구현 옵션
					{  
						breakpoint: 990, //화면 사이즈 960px
						settings: {
							autoplay: false,
							slidesToShow: 2,
							slidesToScroll : 2,
						} 
					}
				]
	});
	$('.sns-youtube-ctrl .btn-pause').click(function() {
		$snsSlider1.slick('slickPause');
		$(this).hide().next().show().focus();
	});
	$('.sns-youtube-ctrl .btn-play').click(function() {
		$snsSlider1.slick('slickPlay');
		$(this).hide().prev().show().focus();
	});

	var $s2stat = $('.sns-facebook-ctrl .slide-count');
	var $snsSlider2 = $('.facebook-slider');
    $snsSlider2.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
        //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
        var i = (currentSlide ? currentSlide : 0) + 1;
        $s2stat.html("<strong>" + "0" + i + "</strong>" + '/' + "0" + slick.slideCount);
    });
	var $snsSlider2 = $('.facebook-slider');
	$snsSlider2.slick({
		autoplay: true,
		autoplaySpeed: 3000,
		slidesToShow : 4,
		slidesToScroll : 4,
		prevArrow: '.sns-facebook-ctrl .btn-prev',
		nextArrow: '.sns-facebook-ctrl .btn-next',
		responsive: [ // 반응형 웹 구현 옵션
					{  
						breakpoint: 990, //화면 사이즈 960px
						settings: {
							slidesToShow: 2,
							slidesToScroll : 2,
							autoplay: false,
						} 
					}
				]
	});
	$('.sns-facebook-ctrl .btn-pause').click(function() {
		$snsSlider2.slick('slickPause');
		$(this).hide().next().show().focus();
	});
	$('.sns-facebook-ctrl .btn-play').click(function() {
		$snsSlider2.slick('slickPlay');
		$(this).hide().prev().show().focus();
	});

	var $s3stat = $('.sns-instagram-ctrl .slide-count');
	var $snsSlider3 = $('.instagram-slider');
    $snsSlider3.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
        //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
        var i = (currentSlide ? currentSlide : 0) + 1;
        $s3stat.html("<strong>" + "0" + i + "</strong>" + '/' + "0" + slick.slideCount);
    });
	var $snsSlider3 = $('.instagram-slider');
	$snsSlider3.slick({
		autoplay: true,
		autoplaySpeed: 3000,
		slidesToShow : 4,
		slidesToScroll : 4,
		prevArrow: '.sns-instagram-ctrl .btn-prev',
		nextArrow: '.sns-instagram-ctrl .btn-next',
		responsive: [ // 반응형 웹 구현 옵션
					{  
						breakpoint: 990, //화면 사이즈 960px
						settings: {
							slidesToShow: 2,
							slidesToScroll : 2,
							autoplay: false,
						} 
					}
				]
	});
	$('.sns-instagram-ctrl .btn-pause').click(function() {
		$snsSlider3.slick('slickPause');
		$(this).hide().next().show().focus();
	});
	$('.sns-instagram-ctrl .btn-play').click(function() {
		$snsSlider3.slick('slickPlay');
		$(this).hide().prev().show().focus();
	});

	$('.target').click(function() {
		$('.sns-dlink-nav li').removeClass('on');
		$(this).addClass('on');
	});
	
	$('.sns-btn-youtube').click(function() {
		$('.sns-cont').removeClass('sns-show');
		$('.sns-youtube').addClass('sns-show');
		$snsSlider1.slick('slickGoTo', 0);
	});
	$('.sns-btn-facebook').click(function() {
		$('.sns-cont').removeClass('sns-show');
		$('.sns-facebook').addClass('sns-show');
		$snsSlider2.slick('slickGoTo', 0);

	});
	$('.sns-btn-instagram').click(function() {
		$('.sns-cont').removeClass('sns-show');
		$('.sns-instagram').addClass('sns-show');
		$snsSlider3.slick('slickGoTo', 0);
	});



	$(".btn-search-detail").on("click",function(){
		$(".detail-box").stop().slideToggle(300);
		$(".search-tit").slideToggle(10);
		$(".search-box").toggleClass('indent');
	});
	$('.m-btn-family a').on('click', function(){
		$('.mobile-tab-group').slideToggle();
	});
	
});

// 의회 소식
$(function() {
	$('.latest dl dt a').bind("mouseenter click focus",function(){
		$(this).parent('dt').addClass('on').siblings('dt').removeClass('on');
		return false;
	});
	
	
	$('#gnb>li:last-child>ul>li:last-child').find(' > a').on('keydown', function (e) {
		 if(e.which == 9 != e.shiftKey && e.which == 9) {
             gnbClose();
         }
    });
	
	$(".sr-onlySkip .last-skip").on("click",function(){
		$("#sMenu").focus();
		return false;
	});
	
	$(".ViewBoxList button.btn").attr("title","새창으로 열림");
	
	
	$(".paginate_button.active").attr("title","현재 게시물 페이지");
	
	//푸터 사이트 바로가기
	$(".select-custom").on("click",function(){
		if($(this).hasClass("on") ){
			$(".select-custom").removeClass("on");
			$(".select-view").removeClass("on");		
		} else {
			$(".select-custom").removeClass("on");
			$(".select-view").removeClass("on");		
			$(this).addClass("on");		
			$(this).next().addClass("on");		
		}
		
	});

	//sns열기
	$(".sns-btn-share").on("click",function(e){
		$(".sns-btns").toggleClass("on");
	});
	$(document).mouseup(function (e){
		var snsPop = $(".sns-btns.on");
		if(snsPop.has(e.target).length === 0){
			snsPop.removeClass("on");
		}
	});
	
});

